Overview
========

Recently I installed Shinobi CE on an old machine with an Intel Core 2 Q6600 @ 2.40GHz CPU and
experienced continuous restarts of the Shinobi Tensorflow CPU plugin. The machine is running
Linux Ubuntu Server 20.04.

Inspection of the [logs](https://pastebin.com/RPc2McGi) gave limited reason for 
the restarts: ```pm2 logs --lines=1000 shinobi-tensorflow```. A [question](https://discord.com/channels/264819784292499457/264819784292499457/707638467538059394) posted on the 
discord channel gives some further background information on the scenario. 


I suspected the CPU in my machine is incompatible with the precompiled libtensorflow library dependency, 
downloaded by the @tensorflow/tfjs-node dependency upon installation. At the time of writing Shinobi installs 
@tensorflow/tfjs-node@1.7.3. This module provides Tensorflow.js for NodeJS. For my machine environment 
this downloads and installs a precompiled C++ library tarball, CPU-linux-1.7.3.tar.gz, containing
libtenstorflow v1.15 and associated C++ header files. This is optimised for CPU usage and 
deployed at /opt/shinobi/node_modules/@tensorflow/tfjs-node/deps.

A past Discord posting suggested compiling the libtensorflow dependency for native machine architecture.
So, further investigation began concerning how to compile a version of libtensorflow optimised for the
CPU of my server.

Using ```lshw -C cpu``` I determined that the CPU is an *Intel Core 2 Quad CPU Q6600 @ 2.40GHz. 

This article documents how I compiled the *libtensorflow* dependency for the 
@tensorflow/tfjs-node module required by the Shinobi Tensorflow plugin. It is hoped that it will be of
some use to other users who are also experiencing similar issues with the Tensorflow (CPU) plugin 
restarting on Shinobi CE in an Ubuntu Linux environment.


Resources
=========
The following resources were consulted for building libtensorflow from source:
- [tfjs-node](https://github.com/tensorflow/tfjs/tree/master/tfjs-node#optional-build-optimal-tensorflow-from-source)
- [tensorflow](https://www.tensorflow.org/install/source)
- [Bazel](https://docs.bazel.build/versions/master/install-ubuntu.html) 


Build Prerequisites
===================

At the time of writing Shinobi installs *@tensorflow/tfjs-node@1.7.3* with *libtensorflow* as
a dependency. It appears that there is a configuration option for specifying which version of the Tensorflow library
to [build](https://www.tensorflow.org/install/source#tensorflow_1x) with Bazel. However, I was uncertain 
which specific minor version of *v1* would be built for the documented example. Subsequently, the decision was made to 
checkout the Tensorflow source code for the exact version (v1.15) installed by *@tensorflow/tfjs-node@1.7.3* for my
machine environment.

The following prerequisites are required for building *libtensorflow v1.15*:
- Bazel v0.26.1
- Python and associated dependencies: pip, six, numpy, wheel, setuptools, mock, future@0.17.1,
keras_applications and keras_preprocessing. Windows builds require Python 2.7 according to this 
[link](https://github.com/caisq/tfjs-node).
- Tensorflow source code for release 1.15.

It is recommended that readers check which libtensorflow library version has been installed by 
@tensorflow/tfjs-node for their machine environment. In my machine environment the library can be found at 
/opt/shinobi/node_modules/@tensorflow/tfjs-node/deps/lib. The version of the library will determine which version
of Bazel to install to perform a build.

Sadly, Bazel 0.26.1 is not available for install from Bazel's apt repository configured in Ubuntu Server 20.04. 
However, Bazel 0.26.1 can be installed using a 
[binary installer](https://docs.bazel.build/versions/master/install-ubuntu.html). I used 
[Bazel 0.26.1](https://github.com/bazelbuild/bazel/releases/tag/0.26.1) which is a patch release. 
So I had to install Bazel 0.26.0 and then 0.26.1 binary installers:

- https://github.com/bazelbuild/bazel/releases/tag/0.26.0
- https://github.com/bazelbuild/bazel/releases/tag/0.26.1

Following this, install Python 3 and package dependencies required by the Bazel build:
``` sh
sudo apt update
sudo apt install python3-dev python3-pip

pip install -U --user pip six numpy wheel setuptools mock 'future>=0.17.1'
pip install -U --user keras_applications --no-deps
pip install -U --user keras_preprocessing --no-deps
```

Finally, download the Tensorflow source code and do a checkout for the required release, 1.15 in my case:
``` sh
git clone https://github.com/tensorflow/tensorflow.git
git checkout r1.15

cd tensorflow
```

Building libtensorflow for CPU optimisation
=================================================
Within the tensorflow source directory configure the build:
```sh
./configure
```

This will prompt for build configuration options. This includes specifying the options
associated with an optimised build that utilises the CPU with Tensorflow, i.e. the 
*--config=opt* option for the Bazel build. 

When prompted for the optimised build configuration, specify the CPU appropriate for your 
environment. This defaults to *-march=native -Wno-sign-compare*. I used  *-march=core2 -Wno-sign-compare*. 
Recognised CPU's associated with the *-march* configuration option are available 
[here](https://gcc.gnu.org/onlinedocs/gcc-4.5.3/gcc/i386-and-x86_002d64-Options.html).

My usecase for Tensorflow was for object detections to be performed using CPU. Subsequently, the remaining
configuration options were unset, i.e. 'N'. Further details of the build configuration are available 
[here](https://www.tensorflow.org/install/source) for those that intend to use Tensorflow optimised
for GPU.

Finally, perform the build by issuing the following command:
``` sh
 bazel build --config=opt --config=monolithic --config=noaws --config=nogcp --config=nohdfs --config=nonccl //tensorflow/tools lib_package:libtensorflow
```

This builds libtensorflow with the following configuration:

- **--config=opt**: Optimised for CPU usage.
- **--config=monolithic**: Static monolithic build.
- **--config=noaws**: Disable Amazon Web Services support.
- **--config=nogcp**: Disable Google Cloud support.
- **--config=nohdfs**: Disable support for Hadoop Distributed File System.
- **--config=nonccl**: Disable Nvidia NCCL support.

It will take a fair amount of time for the build to complete. If possible, build the library on the actual targeted
machine environment. In my case I had to build it on a secondary more powerful linux machine.

Upon completion the build will produce a tarball file at ```bazel-bin/tensorflow/tools/lib_package/libtensorflow.tar.gz file```.
This needs to be extracted into the *deps* folder for *tfjs-node*, e.g. 
*/opt/shinobi/node_modules/@tensorflow/tfjs-node/deps* on my machine.

Try starting up Shinobi and associated plugins and inspect the logs:
``` sh
pm2 restart all
pm2 logs --lines=1000 |more
```

Inspect the status of Shinobi and plugins:
``` sh
pm2 status all
```

At this point the Shinobi Tensorflow Plugin was displayed as online. The Shinobi Tensorflow plugin
logs the following as an error:

```
Hi there. Looks like you are running TensorFlow.js in Node.js. To speed things up dramatically, install our node
backend, which binds to TensorFlow C++ by running npm i @tensorflow/tfjs-node or npm i @tensorflow/tfjs-node-gpu
if you have CUDA. Then call require('@tensorflow/tfjs-node'); (-gpu suffic for CUDA) at the start of your program.
Visit https://github.com/tensorflow/tfjs-node for more details.
```

After posting a [question](https://discord.com/channels/264819784292499457/264819784292499457/710108628962770944) 
this message can be [ignored](https://discord.com/channels/264819784292499457/264819784292499457/710132157472833647) 
for Tensorflow CPU use cases.

Hope this helps others that have encountered similar issues with Intel Core2 and/or other processors incompatible
with the precompiled libtensorflow library installed by @tensorflow/tfjs-node.